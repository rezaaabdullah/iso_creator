*Semua file berekstensi .php didalam folder config_iso ini akan dibaca sebagai file config, 
harap lebih teliti dalam pemberian ekstensi file di folder crete iso.

*pastikan nama variable didalam config sama dengan nama file,
contoh: $EEAB untuk file bernama EEAB.php (case sensitive)

*nama file akan menjadi ID dalam sistem pembuatan ISO ini

*struktur config sebagai berikut
["product_name"] = merupakan nama dari produk yang akan dibuatkan ISO nya

["installer"] = merupakan list installer yang akan dimasukkan ke ISO. strukturnya sebagai berikut
    [
        [
            "url_download" => url file yang akan didownload
            "source_dir" => directory tempat menyimpan file yang didownload
            "destination_dir" => directory tempat file pada ISO
        ]
    ]

    **untuk memasukkan lebih dari satu installer, silahkan gunakan tanda koma (,) sebagai pemisah
    contoh : ["installer] => [["url_download" => "", "source_dir" => "", "destination_dir" => "], ["url_download" => "", "source_dir" => "", "destination_dir" => "]]

    **jika ada file yang diharuskan untuk didownload, silahkan masukan file tersebut kedalam list installer ini.

["manual"] = merupakan list manual yang akan dimasukkan ke ISO. strukturnya sebagai berikut
    [
        [
            "source_dir" => directory tempat menyimpan manual
            "destination_dir" => directory tempat manual pada ISO
        ]
    ]

    **untuk aturan penulisan sama dengan installer, hanya saja tidak ada parameter url_download untuk manual.

["index"] = merupakan file index yang akan dimasukkan ke dalam ISO.
    ** untuk aturan penulisan sama dengan manual

["favicon"] = merupakan file favicon yang akan dimasukkan ke dalam ISO.
    ** untuk aturan penulisan sama dengan manual

["favicon"] = merupakan file favicon yang akan dimasukkan ke dalam ISO.
    ** untuk aturan penulisan sama dengan manual

["autorun"] = merupakan file autorun yang akan dimasukkan ke dalam ISO.
** untuk aturan penulisan sama dengan manual

["js"] = merupakan folder js yang akan dimasukkan ke dalam ISO.
** untuk aturan penulisan sama dengan manual

["css"] = merupakan folder css yang akan dimasukkan ke dalam ISO.
** untuk aturan penulisan sama dengan manual