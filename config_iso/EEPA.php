<?php
$EEPA = 
[
	"product_name" => "ESET Endpoint Protection Advanced",
	"installer" =>
	[
		[
			"url_download" => "",
			"source_dir" => "",
			"destination_dir" => ""
		]
	],
	"manual" =>
	[
		[
			"source_dir" => $manual_dir ."eset_eslc_userguide_enu.pdf",
			"destination_dir" => $iso_component_dir ."ESET/MANUAL/eset_eslc_userguide_enu.pdf"
		]
	],
	"index" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/index.html",
			"destination_dir" => $iso_component_dir ."index.html"
		]
	],
	"favicon" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/favicon.ico",
			"destination_dir" => $iso_component_dir ."favicon.ico"
		]
	],
	"autorun" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/autorun.inf",
			"destination_dir" => $iso_component_dir ."autorun.inf"
		]
	],
	"js" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/js/",
			"destination_dir" => $iso_component_dir ."js/"
		]
	],
	"css" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/css/",
			"destination_dir" => $iso_component_dir ."css/"
		]
	]
];
?>