<?php
$EEAB = 
[
	"product_name" => "ESET Endpoint Antivirus",
	"installer" =>
	[
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/eea/windows/latest/eea_nt32.msi",
			"source_dir" => $installer_endpoint_dir ."Windows/V7/eea_nt32.msi",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/WIN/eea_nt32.msi"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/eea/mac/latest/eea_osx_en.dmg",
			"source_dir" => $installer_endpoint_dir ."Mac/eea_osx_en.dmg",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/MAC/eea_osx_en.dmg"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/eea/windows/latest/eea_nt64.msi",
			"source_dir" => $installer_endpoint_dir ."Windows/V7/eea_nt64.msi",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/WIN/eea_nt64.msi"
		],
		[
			"url_download" => "https://download.eset.com/com/eset/apps/business/eea/linux/g2/latest/eeau.x86_64.bin",
			"source_dir" => $installer_endpoint_dir ."Linux/Desktop/eeau.x86_64.bin",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/LINUX/DESKTOP/eeau.x86_64.bin"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/allinone/latest/x64.zip",
			"source_dir" => $installer_all_in_one_dir ."Setup_x64.zip",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/ALLINONE/Setup_x64.zip"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/agent/latest/agent-linux-i386.sh",
			"source_dir" => $installer_eracomp_dir ."Agent/Linux/Agent-Linux-i386.sh",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Agent/Linux/Agent-Linux-i386.sh"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/agent/latest/agent-linux-x86_64.sh",
			"source_dir" => $installer_eracomp_dir ."Agent/Linux/Agent-Linux-x86_64.sh",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Agent/Linux/Agent-Linux-x86_64.sh"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/agent/latest/agent-macosx-x86_64.dmg",
			"source_dir" => $installer_eracomp_dir ."Agent/Mac/agent-macosx-x86_64.dmg",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Agent/Mac/agent-macosx-x86_64.dmg"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/agent/latest/Agent_x86.msi",
			"source_dir" => $installer_eracomp_dir ."Agent/Windows/Agent_x86.msi",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Agent/Windows/Agent_x86.msi"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/agent/latest/agent_x64.msi",
			"source_dir" => $installer_eracomp_dir ."Agent/Windows/Agent_x64.msi",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Agent/Windows/Agent_x64.msi"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/migrator/latest/win32.zip",
			"source_dir" => $installer_eracomp_dir ."Migration Tool/Windows/migrator_win32.zip",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Migration Tool/Windows/migrator_win32.zip"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/migration_assistant/latest/migrationassistant_win32.zip",
			"source_dir" => $installer_eracomp_dir ."Migration Tool/Windows/migrationassistant_win32.zip",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Migration Tool/Windows/migrationassistant_win32.zip"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/mdm/latest/mdmcore-linux-x86_64.sh",
			"source_dir" => $installer_eracomp_dir ."Mobile Device Connector/Linux/MDMCore-Linux-x86_64.sh",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Mobile Device Connector/Linux/MDMCore-Linux-x86_64.sh"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/mdm/latest/mdmcore_x64.msi",
			"source_dir" => $installer_eracomp_dir ."Mobile Device Connector/Windows/MDMCore_x64.msi",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Mobile Device Connector/Windows/MDMCore_x64.msi"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/rdsensor/latest/rdsensor-linux-i386.sh",
			"source_dir" => $installer_eracomp_dir ."Rogue Detection Sensor/Linux/RDSensor-Linux-i386.sh",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Rogue Detection Sensor/Linux/RDSensor-Linux-i386.sh"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/rdsensor/latest/rdsensor-linux-x86_64.sh",
			"source_dir" => $installer_eracomp_dir ."Rogue Detection Sensor/Linux/RDSensor-Linux-x86_64.sh",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Rogue Detection Sensor/Linux/RDSensor-Linux-x86_64.sh"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/rdsensor/latest/rdsensor_x64.msi",
			"source_dir" => $installer_eracomp_dir ."Rogue Detection Sensor/Windows/RDSensor_x64.msi",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Rogue Detection Sensor/Windows/RDSensor_x64.msi"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/rdsensor/latest/rdsensor_x86.msi",
			"source_dir" => $installer_eracomp_dir ."Rogue Detection Sensor/Windows/RDSensor_x86.msi",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Rogue Detection Sensor/Windows/RDSensor_x86.msi"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/server/windows/latest/server-linux-x86_64.sh",
			"source_dir" => $installer_eracomp_dir ."Server/Linux/Server-Linux-x86_64.sh",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Server/Linux/Server-Linux-x86_64.sh"
		],
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/server/linux/latest/server_x64.msi",
			"source_dir" => $installer_eracomp_dir ."Server/Windows/Server_x64.msi",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/Server/Windows/Server_x64.msi"
		],			
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/era/webconsole/latest/era.war",
			"source_dir" => $installer_eracomp_dir ."WebConsole/era.war",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/WebConsole/era.war"
		]
	],
	"third_party" =>
	[
		[
			"source_dir" => $installer_eracomp_dir ."ThirdParty/Windows/ApacheHttp2.4.20.zip",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/ThirdParty/Windows/Apache HTTP Server with HTTP proxy/ApacheHttp2.4.20.zip"
		],
		[
			"source_dir" => $installer_eracomp_dir ."ThirdParty/Windows/apache-tomcat-7.0.75.exe",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/ThirdParty/Windows/Apache Tomcat/apache-tomcat-7.0.75.exe"
		],
		[
			"source_dir" => $installer_eracomp_dir ."ThirdParty/Windows/SQLEXPR_2008R2_x64_ENU.exe",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/ThirdParty/Windows/SQL Server Express/SQLEXPR_2008R2_x64_ENU.exe"
		],
		[
			"source_dir" => $installer_eracomp_dir ."ThirdParty/Windows/SQLEXPR_2008R2_x86_ENU.exe",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/ThirdParty/Windows/SQL Server Express/SQLEXPR_2008R2_x86_ENU.exe"
		],
		[
			"source_dir" => $installer_eracomp_dir ."ThirdParty/Windows/SQLEXPR_2014_x64_ENU.exe",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/ThirdParty/Windows/SQL Server Express/SQLEXPR_2014_x64_ENU.exe"
		],
		[
			"source_dir" => $installer_eracomp_dir ."ThirdParty/Windows/SQLEXPR_2014_x86_ENU.exe",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/ThirdParty/Windows/SQL Server Express/SQLEXPR_2014_x86_ENU.exe"
		],
		[
			"source_dir" => $installer_eracomp_dir ."ThirdParty/Windows/WinPcap_4_1_3.exe",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ERA/STANDALONE/ThirdParty/Windows/WinPcap/WinPcap_4_1_3.exe"
		]
	],
	"manual" =>
	[
		[
			"source_dir" => $manual_dir ."eset_eslc_userguide_enu.pdf",
			"destination_dir" => $iso_component_dir ."ESET/MANUAL/eset_eslc_userguide_enu.pdf"
		]
	],
	"index" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/index.html",
			"destination_dir" => $iso_component_dir ."index.html"
		]
	],
	"favicon" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/favicon.ico",
			"destination_dir" => $iso_component_dir ."favicon.ico"
		]
	],
	"autorun" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/autorun.inf",
			"destination_dir" => $iso_component_dir ."autorun.inf"
		]
	],
	"js" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/js/",
			"destination_dir" => $iso_component_dir ."js/"
		]
	],
	"css" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/css/",
			"destination_dir" => $iso_component_dir ."css/"
		]
	]
];
?>