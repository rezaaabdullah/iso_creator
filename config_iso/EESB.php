<?php
$EESB = 
[
	"product_name" => "ESET Endpoint Security",
	"installer" =>
	[
		[
			"url_download" => "http://download.eset.com/com/eset/apps/business/ees/windows/latest/ees_nt32.msi",
			"source_dir" => $installer_endpoint_dir ."Windows/V7/ees_nt32.msi",
			"destination_dir" => $iso_component_dir ."ESET/INSTALLER/WIN/ees_nt32.msi"
		],
		// [
		// 	"url_download" => "http://download.eset.com/com/eset/apps/business/ees/windows/latest/ees_nt64.msi",
		// 	"source_dir" => $installer_endpoint_dir ."Windows/V7/ees_nt64.msi",
		// 	"destination_dir" => $iso_component_dir ."ESET/INSTALLER/WIN/ees_nt64.msi"
		// ],
		// [
		// 	"url_download" => "http://download.eset.com/com/eset/apps/business/ees/mac/latest/ees_osx_en.dmg",
		// 	"source_dir" => $installer_endpoint_dir ."Mac/ees_osx_en.dmg",
		// 	"destination_dir" => $iso_component_dir ."ESET/INSTALLER/MAC/ees_osx_en.dmg"
		// ],
		// [
		// 	"url_download" => "http://download.eset.com/com/eset/apps/business/ees/android/latest/ees.apk",
		// 	"source_dir" => $installer_endpoint_dir ."Android/ees.apk",
		// 	"destination_dir" => $iso_component_dir ."ESET/INSTALLER/ANDROID/ees.apk"
		// ]
	],
	"manual" =>
	[
		[
			"source_dir" => $manual_dir ."eset_eslc_userguide_enu.pdf",
			"destination_dir" => $iso_component_dir ."ESET/MANUAL/eset_eslc_userguide_enu.pdf"
		]
	],
	"index" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/index.html",
			"destination_dir" => $iso_component_dir ."index.html"
		]
	],
	"favicon" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/favicon.ico",
			"destination_dir" => $iso_component_dir ."favicon.ico"
		]
	],
	"autorun" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/autorun.inf",
			"destination_dir" => $iso_component_dir ."autorun.inf"
		]
	],
	"js" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/js/",
			"destination_dir" => $iso_component_dir ."js/"
		]
	],
	"css" => 
	[
		[
			"source_dir" => $storage_dir ."OTHERS/css/",
			"destination_dir" => $iso_component_dir ."css/"
		]
	]
];
?>