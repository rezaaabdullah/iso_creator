function is_IP(ip)
{
	var regex = /(^[0-9]{2,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})$/gm;

	var matched = ip.match(regex);

	if (matched)
	{
		return true;
	}

	return false;
}

function validate_private_ip(ip)
{
	var regex = /(^127\.)|(^10\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)|(^192\.168\.)/gm;
	var matched = ip.match(regex);

	if (matched)
	{
		return true;
	}

	return false;
}

function validate_fqdn(str)
{
	var regex = /^(?=.{1,254}$)((?=[a-z0-9-]{1,63}\.)(xn--+)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}$/i;
	var matched = str.match(regex);

	if (matched)
	{
		return true;
	}

	return false;
}

function get_ajax_error(xhr, err)
{
	if (xhr.status != undefined)
	{
	  if(xhr.status == 404)
		{
			return 'Requested URL not found ('+ xhr.status +')';
		}
		else if(xhr.status == 500)
		{
			return 'Internal Server Error ('+ xhr.status +')';
		}
		else if(xhr.status == 504)
		{
			return 'Gateway Timeout ('+ xhr.status +')';
		}
		else
		{
			return 'Response Status Code: ' + xhr.status;
		}
	}
	else if(err == 'parsererror')
	{
		return 'Error. Parsing JSON Request failed.';
	}
	else if(err == 'timeout')
	{
		return 'Request Time out.';
	}
	else
	{
		return 'Unknow Error.\n'+xhr.responseText;
	}
}

function validate_domain(domain)
{
	var regex = /^((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	if (regex.test(domain))
	{
	    return true;
	}

	return false;
}

function validate_email(email)
{
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	if (re.test(email))
	{
			return true;
	}
	else
	{
		return false;
	}
}

/*===================================mail_user.php======================================*/
function select_registered_domain_user(token)
{
	var select_registered_domain_user = $('.select_registered_domain_user').val();

	var dataString = {'select_registered_domain_user' : select_registered_domain_user, 'token' : token};

	if (select_registered_domain_user != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_user.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".response-select-registered-domain-user").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function total_active_user(token, total_active_user, domid)
{
	if (total_active_user == undefined)
	{
		total_active_user = $('.total_active_user').val();
	}

	var dataString = {'total_active_user' : total_active_user, 'domid' : domid, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_user.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".active-user").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function load_profile(token)
{
	var dataString = {'load_profile' : 'load_profile', 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_user.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$(".user-profile").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function change_password(token, data)
{
	var dataString = {'execute_change_password' : 'execute_change_password', 'token' : token, 'data' : data};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_user.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$(".msg-wrapper").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function add_user_step2(token, data)
{
	var dataString = {'add_user_step2' : 'add_user_step2', 'token' : token, 'data' : data};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_user.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$(".msg-wrapper").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function add_user_step1(token, data)
{
	var dataString = {'add_user_step1' : 'add_user_step1', 'token' : token, 'data' : data};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_user.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$(".ResponseModalAddUser").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function get_id_edit_mail_user(token, detailusrcode, domid)
{
	var dataString = {'get_id_edit_mail_user' : 'get_id_edit_mail_user', 'token' : token, 'detailusrcode' : detailusrcode, 'domid' : domid};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_user.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalEditUser").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function edit_user(token, data)
{
	var dataString = {'execute_edit' : 'execute_edit', 'token' : token, 'data' : data};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_user.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$(".msg-wrapper").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function get_id_disable_mail_user(token, data_user, username, domid)
{
	var dataString = {'get_id_disable_mail_user' : 'get_id_disable_mail_user', 'token' : token, 'data' : data_user, 'username' : username, 'domid' : domid};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_user.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalDisableUser").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function disable_user(token, data)
{
	var dataString = {'execute_disable' : 'execute_disable', 'token' : token, 'data' : data};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_user.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$(".msg-wrapper").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

/*===================================mail_domain.php======================================*/
function load_active_domain(token)
{
	var dataString = {'load_active_domain' : 'load_active_domain', 'token' : token};

	$('.response-active-domain').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_domain.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication-mini').remove();
					$(".response-active-domain").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication-mini').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function load_all_domain(token)
{
	var dataString = {'load_all_domain' : 'load_all_domain', 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_domain.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$(".response-all-domain-table").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function total_active_domain(token)
{
	var total_active_domain = $('.total_active_domain').val();

	var dataString = {'total_active_domain' : total_active_domain, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_domain.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".response-active-domain").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function load_pending_domain(token)
{
	var dataString = {'load_pending_domain' : 'load_pending_domain', 'token' : token};

	$('.response-pending-domain').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_domain.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication-mini').remove();
					$(".response-pending-domain").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication-mini').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function total_pending_domain(token)
{
	var total_pending_domain = $('.total_pending_domain').val();

	var dataString = {'total_pending_domain' : total_pending_domain, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_domain.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".response-pending-domain").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function total_inactive_domain(token)
{
	var total_inactive_domain = $('.total_inactive_domain').val();

	var dataString = {'total_inactive_domain' : total_inactive_domain, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_domain.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".response-inactive-domain").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function get_id_edit_mail_domain(token, data_domain, dom_name)
{
	var dataString = {'get_id_edit_mail_domain' : 'get_id_edit_mail_domain', 'token' : token, 'data' : data_domain, 'dom_name' : dom_name};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_domain.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalEditDomain").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function edit_mail_domain(token, data)
{
	var dataString = {'execute_edit' : 'execute_edit', 'token' : token, 'data' : data};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_domain.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$(".msg-wrapper").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function preparing_transfer_master_domain(token, domid)
{
	var dataString = {'preparing_transfer_master_domain' : 'true', 'token' : token, 'domid' : domid};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_domain.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$(".response-get-transfer-master-candidate").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function submit_transfer_master_domain(token, data)
{
	var dataString = {'submit_transfer_master_domain' : 'true', 'token' : token, 'data' : data};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_domain.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$("#ResponseModalNotification").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function preparing_test_email(token, domname)
{
	var dataString = {'preparing_test_email' : 'true', 'token' : token, 'domname' : domname};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_domain.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$(".response-get-domain-test-email").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function submit_test_email(token, data)
{
	var dataString = {'submit_test_email' : 'true', 'token' : token, 'data' : data};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_domain.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$("#ResponseModalNotification").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function add_mail_domain(token, data)
{
	var dataString = {'execute_add' : 'execute_add', 'token' : token, 'data' : data};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_domain.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$(".msg-wrapper").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function confirmation_mail_domain(token, data)
{
	var dataString = {'execute_confirmation' : 'execute_confirmation', 'token' : token, 'data' : data};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "mail_domain.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$("#ResponseModalNotification").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function resend_confirmation_code(domid, token)
{
	var dataString = {"resend_confirmation_code" : "true", 'token' : token, "domid" : domid};
	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
		$.ajax({
		type: "POST",
		url: "mail_domain.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$("#ResponseModalNotification").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

/*===================================transaction_history.php======================================*/
function load_active_domain_transaction_history(token)
{
	var dataString = {'load_active_domain_transaction_history' : 'load_active_domain_transaction_history', 'token' : token};

	$('.response-all-domain-table').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "transaction_history.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication-mini').remove();
					$(".response-all-domain-table").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication-mini').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function total_active_domain_transaction_history(token)
{
	var total_active_domain_transaction_history = $('.total_active_domain_transaction_history').val();

	var dataString = {'total_active_domain_transaction_history' : total_active_domain_transaction_history, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "transaction_history.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".response-active-domain").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function get_transaction_history(token, domid, domname)
{
	var dataString = {'get_transaction_history' : 'get_transaction_history', 'token' : token, 'domid' : domid, 'domname' : domname};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
		$.ajax({
			type: "POST",
			url: "transaction_history.php",
			data: dataString,
			cache: false,
			success: function(data)
			{
				$('.loading-indication').remove();
				$("#ResponseModalGetTransactionHistory").html(data);
			},
			error: function(xhr, err)
			{
				$('.loading-indication').remove();
				get_ajax_error(xhr, err);
			}
	});
}

/*===================================smtp_auth.php======================================*/
function load_all_smtp_auth(token)
{
	var dataString = {'load_all_smtp_auth' : 'load_all_smtp_auth', 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
		type: "POST",
		url: "smtp_auth.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$(".response-all-smtp-table").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function load_active_smtp_auth(token)
{
	var dataString = {'load_active_smtp_auth' : 'load_active_smtp_auth', 'token' : token};

	$('.response-active-smtp-auth').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "smtp_auth.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication-mini').remove();
					$(".response-active-smtp-auth").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication-mini').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function total_active_smtp_auth(token)
{
	var total_active_smtp_auth = $('.total_active_smtp_auth').val();

	var dataString = {'total_active_smtp_auth' : total_active_smtp_auth, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "smtp_auth.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".response-active-smtp-auth").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function load_inactive_smtp_auth(token)
{
	var dataString = {'load_inactive_smtp_auth' : 'load_inactive_smtp_auth', 'token' : token};

	$('.response-inactive-smtp-auth').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "smtp_auth.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication-mini').remove();
					$(".response-inactive-smtp-auth").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication-mini').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function total_inactive_smtp_auth(token)
{
	var total_inactive_smtp_auth = $('.total_inactive_smtp_auth').val();

	var dataString = {'total_inactive_smtp_auth' : total_inactive_smtp_auth, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "smtp_auth.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".response-inactive-smtp-auth").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

/*===================================mail_bw_domain.php======================================*/
function select_white_domain(token)
{
	var select_white_domain = $('.select_white_domain').val();

	var dataString = {'select_white_domain' : select_white_domain, 'token' : token};

	if (select_white_domain != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_bw_domain.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".response-whitelist").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function search_white_domain(token)
{
	$('.select-all-whitelist').prop('checked', false);

	var dataString = {'search_white_domain': $('#search_white_domain_txt').val(),'select_white_domain_for_search' : $('.select_white_domain').val(), 'token' : token};

	if ($('.select_white_domain').val() != '')
	{
		$('.response-search-whitelist-domain').empty();
		$('.response-search-whitelist-domain').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_bw_domain.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication-mini').remove();
						$(".response-search-whitelist-domain").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication-mini').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function total_whitelist(token)
{
	$('.select-all-whitelist').prop('checked', false);

	var total_whitelist = $('.total_whitelist').val();

	if ($('#search_white_domain_txt').val() != '')
	{
		var dataString = {'total_whitelist' : total_whitelist, 'search_white_domain_txt' : $('#search_white_domain_txt').val(), 'token' : token};
	}
	else
	{
		var dataString = {'total_whitelist' : total_whitelist, 'token' : token};
	}

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_bw_domain.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".response-total-whitelist-domain").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function submit_add_white_domain(token)
{
	var white_domain_name = $('#white_domain_name').val();
	var select_white_domain = $('.select_white_domain').val();

	var dataString = {'submit_add_white_domain' : 'true','white_domain_name' : white_domain_name, 'selected_white_domain' : select_white_domain, 'token' : token};

	if (white_domain_name != '')
	{
    $.ajax({
			type: "POST",
			url: "mail_bw_domain.php",
			data: dataString,
			cache: false,
			success: function(data, event)
			{
				$(".msg-wrapper").html(data);
			},
			error: function(xhr, err)
			{
				get_ajax_error(xhr, err);
			}
		});
  }
}

function remove_whitelist_domain(token, selected_white_domain)
{
	var domain_checked = [];
	var join_domain_selected;

	$(".domain-check-whitelist:checked").each(function() {
		domain_checked.push($(this).val());
	});

	join_domain_selected = domain_checked.join(', ') ;

	if(join_domain_selected.length > 0)
	{
		var dataString = {'remove_whitelist_domain' : 'remove_whitelist_domain', 'selected_white_domain' : selected_white_domain, 'domain_list' : join_domain_selected, 'token' : token};
	}

	else
	{
		var dataString = {'remove_whitelist_domain' : 'remove_whitelist_domain', 'domain_list' : 'null', 'token' : token};
	}

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
  $.ajax({
		type: "POST",
		url: "mail_bw_domain.php",
		data: dataString,
		cache: false,
		success: function(data)
		{
			$('.loading-indication').remove();
			$("#ResponseRemoveDisableDomainWhiteList").html(data);
		},
		error: function(xhr, err)
		{
			$('.loading-indication').remove();
			get_ajax_error(xhr, err);
		}
	});
}

function submit_remove_white_domain(token)
{
	var delete_domain_whitelist = $('#delete_domain_whitelist').val();
	var select_white_domain = $('.select_white_domain').val();

	var dataString = {'submit_remove_white_domain' : 'true','delete_domain_whitelist' : delete_domain_whitelist, 'selected_white_domain' : select_white_domain, 'token' : token};

	if (delete_domain_whitelist != '')
	{
    $.ajax({
			type: "POST",
			url: "mail_bw_domain.php",
			data: dataString,
			cache: false,
			success: function(data, event)
			{
				$(".msg-wrapper").html(data);
			},
			error: function(xhr, err)
			{
				get_ajax_error(xhr, err);
			}
		});
  }
}

function search_black_domain(token)
{
	$('.select-all-blacklist').prop('checked', false);

	var dataString = {'search_black_domain': $('#search_black_domain_txt').val(),'select_black_domain_for_search' : $('.select_black_domain').val(), 'token' : token};

	if ($('.select_black_domain').val() != '')
	{
		$('.response-search-blacklist-domain').empty();
		$('.response-search-blacklist-domain').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_bw_domain.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication-mini').remove();
						$(".response-search-blacklist-domain").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication-mini').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_black_domain(token)
{
	var select_black_domain = $('.select_black_domain').val();

	var dataString = {'select_black_domain' : select_black_domain, 'token' : token};

	if (select_black_domain != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_bw_domain.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".response-blacklist").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function total_blacklist(token)
{
	$('.select-all-blacklist').prop('checked', false);

	var total_blacklist = $('.total_blacklist').val();

	if ($('#search_black_domain_txt').val() != '')
	{
		var dataString = {'total_blacklist' : total_blacklist, 'search_black_domain_txt' : $('#search_black_domain_txt').val(), 'token' : token};
	}
	else
	{
		var dataString = {'total_blacklist' : total_blacklist, 'token' : token};
	}

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_bw_domain.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".response-total-blacklist-domain").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}


function submit_add_black_domain(token)
{
	var black_domain_name = $('#black_domain_name').val();
	var select_black_domain = $('.select_black_domain').val();

	var dataString = {'submit_add_black_domain' : 'true','black_domain_name' : black_domain_name, 'selected_black_domain' : select_black_domain, 'token' : token};

	if (black_domain_name != '')
	{
    $.ajax({
			type: "POST",
			url: "mail_bw_domain.php",
			data: dataString,
			cache: false,
			success: function(data, event)
			{
				$(".msg-wrapper").html(data);
			},
			error: function(xhr, err)
			{
				get_ajax_error(xhr, err);
			}
		});
  }
}

function remove_blacklist_domain(token, selected_black_domain)
{
	var domain_checked = [];
	var join_domain_selected;

	$(".domain-check-blacklist:checked").each(function() {
		domain_checked.push($(this).val());
	});

	join_domain_selected = domain_checked.join(', ') ;

	if(join_domain_selected.length > 0)
	{
		var dataString = {'remove_blacklist_domain' : 'remove_blacklist_domain', 'selected_black_domain' : selected_black_domain, 'domain_list' : join_domain_selected, 'token' : token};
	}

	else
	{
		var dataString = {'remove_blacklist_domain' : 'remove_blacklist_domain', 'domain_list' : 'null', 'token' : token};
	}


		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_bw_domain.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseRemoveDisableDomainBlackList").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function submit_remove_black_domain(token)
{
	var delete_domain_blacklist = $('#delete_domain_blacklist').val();
	var select_black_domain = $('.select_black_domain').val();

	var dataString = {'submit_remove_black_domain' : 'true','delete_domain_blacklist' : delete_domain_blacklist, 'selected_black_domain' : select_black_domain, 'token' : token};

	if (delete_domain_blacklist != '')
	{
    $.ajax({
			type: "POST",
			url: "mail_bw_domain.php",
			data: dataString,
			cache: false,
			success: function(data, event)
			{
				$(".msg-wrapper").html(data);
			},
			error: function(xhr, err)
			{
				get_ajax_error(xhr, err);
			}
		});
  }
}


/*===================================mail_bw_email.php======================================*/
function search_white_email(token)
{
	$('.select-all-whitelist').prop('checked', false);

	var dataString = {'search_white_email': $('#search_white_email_txt').val(),'select_white_email_for_search' : $('.select_white_email').val(), 'token' : token};

	if ($('.select_white_email').val() != '')
	{
		$('.response-search-whitelist-email').empty();
		$('.response-search-whitelist-email').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_bw_email.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication-mini').remove();
						$(".response-search-whitelist-email").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication-mini').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_white_email(token)
{
	var select_white_email = $('.select_white_email').val();

	var dataString = {'select_white_email' : select_white_email, 'token' : token};

	if (select_white_email != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_bw_email.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".response-whitelist").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function total_whitelist_email(token)
{
	$('.select-all-whitelist').prop('checked', false);

	var total_whitelist = $('.total_whitelist').val();

	if ($('#search_white_email_txt').val() != '')
	{
		var dataString = {'total_whitelist' : total_whitelist, 'search_white_email_txt' : $('#search_white_email_txt').val(), 'token' : token};
	}
	else
	{
		var dataString = {'total_whitelist' : total_whitelist, 'token' : token};
	}

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_bw_email.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".response-total-whitelist-email").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function submit_add_white_email(token)
{
	var white_email_name = $('#white_email_name').val();
	var select_white_email = $('.select_white_email').val();

	var dataString = {'submit_add_white_email' : 'true','white_email_name' : white_email_name, 'selected_white_email' : select_white_email, 'token' : token};

	if (white_email_name != '')
	{
    $.ajax({
			type: "POST",
			url: "mail_bw_email.php",
			data: dataString,
			cache: false,
			success: function(data, event)
			{
				$(".msg-wrapper").html(data);
			},
			error: function(xhr, err)
			{
				get_ajax_error(xhr, err);
			}
		});
  }
}

function remove_whitelist_email(token, selected_white_email)
{
	var email_checked = [];
	var join_email_selected;

	$(".email-check-whitelist:checked").each(function() {
		email_checked.push($(this).val());
	});

	join_email_selected = email_checked.join(', ') ;


	if(join_email_selected.length > 0)
	{
		var dataString = {'remove_whitelist_email' : 'remove_whitelist_email', 'selected_white_email' : selected_white_email, 'email_list' : join_email_selected, 'token' : token};
	}

	else
	{
		var dataString = {'remove_whitelist_email' : 'remove_whitelist_email', 'email_list' : 'null', 'token' : token};
	}


		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_bw_email.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseRemoveDisableEmailWhiteList").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function submit_remove_white_email(token)
{
	var delete_email_whitelist = $('#delete_email_whitelist').val();
	var select_white_email = $('.select_white_email').val();

	var dataString = {'submit_remove_white_email' : 'true','delete_email_whitelist' : delete_email_whitelist, 'selected_white_email' : select_white_email, 'token' : token};

	if (delete_email_whitelist != '')
	{
    $.ajax({
			type: "POST",
			url: "mail_bw_email.php",
			data: dataString,
			cache: false,
			success: function(data, event)
			{
				$(".msg-wrapper").html(data);
			},
			error: function(xhr, err)
			{
				get_ajax_error(xhr, err);
			}
		});
  }
}

function search_black_email(token)
{
	$('.select-all-blacklist').prop('checked', false);

	var dataString = {'search_black_email': $('#search_black_email_txt').val(),'select_black_email_for_search' : $('.select_black_email').val(), 'token' : token};

	if ($('.select_black_email').val() != '')
	{
		$('.response-search-blacklist-email').empty();
		$('.response-search-blacklist-email').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_bw_email.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication-mini').remove();
						$(".response-search-blacklist-email").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication-mini').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_black_email(token)
{
	var select_black_email = $('.select_black_email').val();

	var dataString = {'select_black_email' : select_black_email, 'token' : token};

	if (select_black_email != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_bw_email.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".response-blacklist").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function total_blacklist_email(token)
{
	$('.select-all-blacklist').prop('checked', false);

	var total_blacklist = $('.total_blacklist').val();

	if ($('#search_black_email_txt').val() != '')
	{
		var dataString = {'total_blacklist' : total_blacklist, 'search_black_email_txt' : $('#search_black_email_txt').val(), 'token' : token};
	}
	else
	{
		var dataString = {'total_blacklist' : total_blacklist, 'token' : token};
	}

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_bw_email.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".response-total-blacklist-email").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function submit_add_black_email(token)
{
	var black_email_name = $('#black_email_name').val();
	var select_black_email = $('.select_black_email').val();

	var dataString = {'submit_add_black_email' : 'true','black_email_name' : black_email_name, 'selected_black_email' : select_black_email, 'token' : token};

	if (black_email_name != '')
	{
    $.ajax({
			type: "POST",
			url: "mail_bw_email.php",
			data: dataString,
			cache: false,
			success: function(data, event)
			{
				$(".msg-wrapper").html(data);
			},
			error: function(xhr, err)
			{
				get_ajax_error(xhr, err);
			}
		});
  }
}

function remove_blacklist_email(token, selected_black_email)
{
	var email_checked = [];
	var join_email_selected;

	$(".email-check-blacklist:checked").each(function() {
		email_checked.push($(this).val());
	});

	join_email_selected = email_checked.join(', ') ;

	if(join_email_selected.length > 0)
	{
		var dataString = {'remove_blacklist_email' : 'remove_blacklist_email', 'selected_black_email' : selected_black_email, 'email_list' : join_email_selected, 'token' : token};
	}

	else
	{
		var dataString = {'remove_blacklist_email' : 'remove_blacklist_email', 'domain_list' : 'null', 'token' : token};
	}


		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_bw_email.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseRemoveDisableEmailBlackList").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function submit_remove_black_email(token)
{
	var delete_email_blacklist = $('#delete_email_blacklist').val();
	var select_black_email = $('.select_black_email').val();

	var dataString = {'submit_remove_black_email' : 'true','delete_email_blacklist' : delete_email_blacklist, 'selected_black_email' : select_black_email, 'token' : token};

	if (delete_email_blacklist != '')
	{
    $.ajax({
			type: "POST",
			url: "mail_bw_email.php",
			data: dataString,
			cache: false,
			success: function(data, event)
			{
				$(".msg-wrapper").html(data);
			},
			error: function(xhr, err)
			{
				get_ajax_error(xhr, err);
			}
		});
  }
}

function search_white_attachment(token)
{
	$('.select-all-whitelist').prop('checked', false);

	var dataString = {'search_white_attachment': $('#search_white_attachment_txt').val(),'select_white_attachment_for_search' : $('.select_white_attachment').val(), 'token' : token};

	if ($('.select_white_attachment').val() != '')
	{
		$('.response-search-whitelist-attachment').empty();
		$('.response-search-whitelist-attachment').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_bw_attachment.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication-mini').remove();
						$(".response-search-whitelist-attachment").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication-mini').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_white_attachment(token)
{
	var select_white_attachment = $('.select_white_attachment').val();

	var dataString = {'select_white_attachment' : select_white_attachment, 'token' : token};

	if (select_white_attachment != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_bw_attachment.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".response-whitelist").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function total_whitelist_attachment(token)
{
	$('.select-all-whitelist').prop('checked', false);

	var total_whitelist = $('.total_whitelist').val();

	if ($('#search_white_attachment_txt').val() != '')
	{
		var dataString = {'total_whitelist' : total_whitelist, 'search_white_attachment_txt' : $('#search_white_attachment_txt').val(), 'token' : token};
	}
	else
	{
		var dataString = {'total_whitelist' : total_whitelist, 'token' : token};
	}

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_bw_attachment.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".response-total-whitelist-attachment").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function submit_add_white_attachment(token)
{
	var data = $('#form-add-whitelist').serializeArray();
	var select_white_attachment = $('.select_white_attachment').val();

	var dataString = {'submit_add_white_attachment' : 'true','data' : data, 'selected_white_attachment' : select_white_attachment, 'token' : token};

	if (data != '')
	{
    $.ajax({
			type: "POST",
			url: "mail_bw_attachment.php",
			data: dataString,
			cache: false,
			success: function(data, event)
			{
				$(".msg-wrapper").html(data);
			},
			error: function(xhr, err)
			{
				get_ajax_error(xhr, err);
			}
		});
  }
}

function remove_whitelist_attachment(token, selected_white_attachment)
{
	var attachment_checked = [];
	var join_attachment_selected;

	$(".attachment-check-whitelist:checked").each(function() {
		attachment_checked.push($(this).val());
	});

	join_attachment_selected = attachment_checked.join(', ') ;


	if(join_attachment_selected.length > 0)
	{
		var dataString = {'remove_whitelist_attachment' : 'remove_whitelist_attachment', 'selected_white_attachment' : selected_white_attachment, 'attachment_list' : join_attachment_selected, 'token' : token};
	}

	else
	{
		var dataString = {'remove_whitelist_attachment' : 'remove_whitelist_attachment', 'attachment_list' : 'null', 'token' : token};
	}


		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_bw_attachment.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseRemoveDisableAttachmentWhiteList").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function submit_remove_white_attachment(token)
{
	var delete_attachment_whitelist = $('#delete_attachment_whitelist').val();
	var select_white_attachment = $('.select_white_attachment').val();

	var dataString = {'submit_remove_white_attachment' : 'true','delete_attachment_whitelist' : delete_attachment_whitelist, 'selected_white_attachment' : select_white_attachment, 'token' : token};

	if (delete_attachment_whitelist != '')
	{
    $.ajax({
			type: "POST",
			url: "mail_bw_attachment.php",
			data: dataString,
			cache: false,
			success: function(data, event)
			{
				$(".msg-wrapper").html(data);
			},
			error: function(xhr, err)
			{
				get_ajax_error(xhr, err);
			}
		});
  }
}

function search_black_attachment(token)
{
	$('.select-all-blacklist').prop('checked', false);

	var dataString = {'search_black_attachment': $('#search_black_attachment_txt').val(),'select_black_attachment_for_search' : $('.select_black_attachment').val(), 'token' : token};

	if ($('.select_black_attachment').val() != '')
	{
		$('.response-search-blacklist-attachment').empty();
		$('.response-search-blacklist-attachment').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_bw_attachment.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication-mini').remove();
						$(".response-search-blacklist-attachment").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication-mini').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_black_attachment(token)
{
	var select_black_attachment = $('.select_black_attachment').val();

	var dataString = {'select_black_attachment' : select_black_attachment, 'token' : token};

	if (select_black_attachment != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_bw_attachment.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".response-blacklist").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function total_blacklist_attachment(token)
{
	$('.select-all-blacklist').prop('checked', false);

	var total_blacklist = $('.total_blacklist').val();

	if ($('#search_black_attachment_txt').val() != '')
	{
		var dataString = {'total_blacklist' : total_blacklist, 'search_black_attachment_txt' : $('#search_black_attachment_txt').val(), 'token' : token};
	}
	else
	{
		var dataString = {'total_blacklist' : total_blacklist, 'token' : token};
	}

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_bw_attachment.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$(".response-total-blacklist-attachment").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function submit_add_black_attachment(token)
{
	var data = $('#form-add-blacklist').serializeArray();
	var select_black_attachment = $('.select_black_attachment').val();

	var dataString = {'submit_add_black_attachment' : 'true','data' : data, 'selected_black_attachment' : select_black_attachment, 'token' : token};

	if (data != '')
	{
    $.ajax({
			type: "POST",
			url: "mail_bw_attachment.php",
			data: dataString,
			cache: false,
			success: function(data, event)
			{
				$(".msg-wrapper").html(data);
			},
			error: function(xhr, err)
			{
				get_ajax_error(xhr, err);
			}
		});
  }
}

function remove_blacklist_attachment(token, selected_black_attachment)
{
	var attachment_checked = [];
	var join_attachment_selected;

	$(".attachment-check-blacklist:checked").each(function() {
		attachment_checked.push($(this).val());
	});

	join_attachment_selected = attachment_checked.join(', ') ;

	if(join_attachment_selected.length > 0)
	{
		var dataString = {'remove_blacklist_attachment' : 'remove_blacklist_attachment', 'selected_black_attachment' : selected_black_attachment, 'attachment_list' : join_attachment_selected, 'token' : token};
	}

	else
	{
		var dataString = {'remove_blacklist_attachment' : 'remove_blacklist_attachment', 'domain_list' : 'null', 'token' : token};
	}


		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_bw_attachment.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseRemoveDisableAttachmentBlackList").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function submit_remove_black_attachment(token)
{
	var delete_attachment_blacklist = $('#delete_attachment_blacklist').val();
	var select_black_attachment = $('.select_black_attachment').val();

	var dataString = {'submit_remove_black_attachment' : 'true','delete_attachment_blacklist' : delete_attachment_blacklist, 'selected_black_attachment' : select_black_attachment, 'token' : token};

	if (delete_attachment_blacklist != '')
	{
    $.ajax({
			type: "POST",
			url: "mail_bw_attachment.php",
			data: dataString,
			cache: false,
			success: function(data, event)
			{
				$(".msg-wrapper").html(data);
			},
			error: function(xhr, err)
			{
				get_ajax_error(xhr, err);
			}
		});
  }
}

/*===================================mail_quarantine.php======================================*/
function select_quarantine_mail(token)
{
	var select_quarantine_mail = $('.select_quarantine_mail').val();

	var dataString = {'select_quarantine_mail' : select_quarantine_mail, 'token' : token};

	if (select_quarantine_mail != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_quarantine.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".response-select-quarantine-mail").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function date_quarantine_mail(token)
{
	$('.select-all').prop('checked', false);

	var date_quarantine_mail = $('.date_quarantine_mail').val();

	var dataString = {'date_quarantine_mail' : date_quarantine_mail, 'token' : token};

		$('.date-total-quarantine-mail').empty();
		$('.date-total-quarantine-mail').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');

        $.ajax({
				type: "POST",
				url: "mail_quarantine.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication-mini').remove();
					$(".date-total-quarantine-mail").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication-mini').remove();
					get_ajax_error(xhr, err);
				}
		});

}

function total_quarantine_mail(token)
{
	$('.select-all').prop('checked', false);

	var total_quarantine_mail = $('.total_quarantine_mail').val();

	var dataString = {'total_quarantine_mail' : total_quarantine_mail, 'token' : token};

	$('.response-total-quarantine-mail').empty();
	$('.response-total-quarantine-mail').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication-mini').remove();
					$(".response-total-quarantine-mail").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication-mini').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function delete_mail_quarantine(token, selected_quarantine_email, dom_id)
{
	var email_checked = [];
	var join_email_selected;

	$(".email-check:checked").each(function() {
		email_checked.push($(this).val());
	});

	join_email_selected = email_checked.join(', ') ;

	if(join_email_selected.length > 0)
	{
		var dataString = {'delete_mail_quarantine' : 'delete_mail_quarantine', 'selected_quarantine_email' : selected_quarantine_email, 'dom_id' : dom_id, 'email_list' : join_email_selected, 'token' : token};
	}

	else
	{
		var dataString = {'delete_mail_quarantine' : 'delete_mail_quarantine', 'domain_list' : 'null', 'token' : token};
	}

		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalDisableDomain").html(data);
					$('#ModalDisableDomain').modal('show');
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function execute_delete_mail_quarantine(token, email_list)
{
	var dataString = {'execute_delete_mail_quarantine' : 'execute_delete_mail_quarantine', 'email_list' : email_list, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
			type: "POST",
			url: "mail_quarantine.php",
			data: dataString,
			cache: false,
			success: function(data)
			{
				$('.loading-indication').remove();
				$("#ResponseModalDisableDomain").html(data);
			},
			error: function(xhr, err)
			{
				$('.loading-indication').remove();
				get_ajax_error(xhr, err);
			}
	});
}

function release_mail_quarantine(token, selected_quarantine_email, dom_id)
{
	var email_checked = [];
	var join_email_selected;

	$(".email-check:checked").each(function() {
		email_checked.push($(this).val());
	});

	join_email_selected = email_checked.join(', ') ;

	if(join_email_selected.length > 0)
	{
		var dataString = {'release_mail_quarantine' : 'release_mail_quarantine', 'selected_quarantine_email' : selected_quarantine_email, 'dom_id' : dom_id, 'email_list' : join_email_selected, 'token' : token};
	}

	else
	{
		var dataString = {'release_mail_quarantine' : 'release_mail_quarantine', 'domain_list' : 'null', 'token' : token};
	}

		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalDisableDomain").html(data);
					$('#ModalDisableDomain').modal('show');
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function execute_release_mail_quarantine(token, email_list, dom_name, dom_id)
{
	var dataString = {'execute_release_mail_quarantine' : 'execute_release_mail_quarantine', 'email_list' : email_list, 'dom_name' : dom_name, 'dom_id' : dom_id, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
			type: "POST",
			url: "mail_quarantine.php",
			data: dataString,
			cache: false,
			success: function(data)
			{
				$('.loading-indication').remove();
				$("#ResponseModalDisableDomain").html(data);
			},
			error: function(xhr, err)
			{
				$('.loading-indication').remove();
				get_ajax_error(xhr, err);
			}
	});
}

function add_domain_quarantine_to_whitelist(token, selected_quarantine_email, dom_id)
{
	var email_checked = [];
	var join_email_selected;

	$(".email-check:checked").each(function() {
		email_checked.push($(this).val());
	});

	join_email_selected = email_checked.join(', ') ;

	if(join_email_selected.length > 0)
	{
		var dataString = {'add_domain_quarantine_to_whitelist' : 'true', 'selected_quarantine_email' : selected_quarantine_email,'dom_id' : dom_id, 'domain_list' : join_email_selected, 'token' : token};
	}

	else
	{
		var dataString = {'add_domain_quarantine_to_whitelist' : 'true', 'domain_list' : 'null', 'token' : token};
	}

		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalDisableDomain").html(data);
					$('#ModalDisableDomain').modal('show');
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function add_domain_quarantine_to_whitelist_on_release(token, selected_quarantine_email, dom_id)
{
	var email_checked = [];
	var join_email_selected;

	$(".domain_list:checked").each(function() {
		email_checked.push($(this).val());
	});

	join_email_selected = email_checked.join(', ') ;

	if(join_email_selected.length > 0)
	{
		var dataString = {'execute_add_domain_quarantine_to_whitelist' : 'true', 'selected_quarantine_domain' : selected_quarantine_email, 'domain_list' : join_email_selected, "dom_id" : dom_id, 'token' : token};
		$('#col_domain').prepend('<div class="loading-indication-mini"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication-mini').remove();
					$("#col_domain").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication-mini').remove();
					get_ajax_error(xhr, err);
				}
		});
	}
}

function execute_add_domain_quarantine_to_whitelist(token, domain_list, dom_id)
{
	$(".select-all").prop('checked', false);

	var dataString = {'execute_add_domain_quarantine_to_whitelist' : 'true', 'domain_list' : domain_list, "dom_id" : dom_id, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
			type: "POST",
			url: "mail_quarantine.php",
			data: dataString,
			cache: false,
			success: function(data)
			{
				$('.loading-indication').remove();
				$("#ResponseModalDisableDomain").html(data);
			},
			error: function(xhr, err)
			{
				$('.loading-indication').remove();
				get_ajax_error(xhr, err);
			}
	});
}

function add_email_quarantine_to_whitelist(token, selected_quarantine_email, dom_id)
{
	var email_checked = [];
	var join_email_selected;

	$(".email-check:checked").each(function() {
		email_checked.push($(this).val());
	});

	join_email_selected = email_checked.join(', ') ;

	if(join_email_selected.length > 0)
	{
		var dataString = {'add_email_quarantine_to_whitelist' : 'true', 'selected_quarantine_email' : selected_quarantine_email,'dom_id' : dom_id, 'email_list' : join_email_selected, 'token' : token};
	}

	else
	{
		var dataString = {'add_email_quarantine_to_whitelist' : 'true', 'email_list' : 'null', 'token' : token};
	}

		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalDisableDomain").html(data);
					$('#ModalDisableDomain').modal('show');
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function add_email_quarantine_to_whitelist_on_release(token, selected_quarantine_email, dom_id)
{
	var email_checked = [];
	var join_email_selected;

	$(".mail_list:checked").each(function() {
		email_checked.push($(this).val());
	});

	join_email_selected = email_checked.join(', ') ;

	if(join_email_selected.length > 0)
	{
		var dataString = {'execute_add_email_quarantine_to_whitelist' : 'true', 'selected_quarantine_email' : selected_quarantine_email, 'dom_id' : dom_id, 'email_list' : join_email_selected, 'token' : token};
		$('#col_mail').prepend('<div class="loading-indication-mini"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication-mini').remove();
					$("#col_mail").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication-mini').remove();
					get_ajax_error(xhr, err);
				}
		});
	}
}

function execute_add_email_quarantine_to_whitelist(token, email_list, dom_id)
{
	$(".select-all").prop('checked', false);

	var dataString = {'execute_add_email_quarantine_to_whitelist' : 'true', 'email_list' : email_list, "dom_id" : dom_id, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
			type: "POST",
			url: "mail_quarantine.php",
			data: dataString,
			cache: false,
			success: function(data)
			{
				$('.loading-indication').remove();
				$("#ResponseModalDisableDomain").html(data);
			},
			error: function(xhr, err)
			{
				$('.loading-indication').remove();
				get_ajax_error(xhr, err);
			}
	});
}

function view_mail_quarantine(token, msg_id)
{
	if(msg_id)
	{
		var dataString = {'view_mail_quarantine' : 'view_mail_quarantine', 'msg_id' : msg_id, 'token' : token};
	}
	else
	{
		var dataString = {'view_mail_quarantine' : 'view_mail_quarantine', 'msg_id' : 'null', 'token' : token};
	}

		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalDisableDomain").html(data);
					$('#ModalDisableDomain').modal('show');
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

/*===================================mail_quarantine_search.php======================================*/

function search_quarantine_mail(token)
{
	$('.select-all').prop('checked', false);

	var search_param = $('.form-search-mail').serializeArray();

	var dataString = {'search_quarantine_mail' : 'true', 'search_param' : search_param, 'token' : token};

		$('.response-search-quarantine-mail').empty();
		$('.response-search-quarantine-mail').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');

        $.ajax({
				type: "POST",
				url: "mail_quarantine_search.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication-mini').remove();
					$(".response-search-quarantine-mail").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication-mini').remove();
					get_ajax_error(xhr, err);
				}
		});

}

function total_search_quarantine_mail(token)
{
	$('.select-all').prop('checked', false);

	var total_search_quarantine_mail = $('.total_quarantine_mail').val();

	var dataString = {'total_search_quarantine_mail' : total_search_quarantine_mail, 'search_param' : $('.form-search-mail').serializeArray(), 'token' : token};

	$('.response-total-quarantine-mail').empty();
	$('.response-total-quarantine-mail').prepend('<div class="loading-indication-mini"><img src="assets/img/loading-mini.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine_search.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication-mini').remove();
					$(".response-total-quarantine-mail").html(data);
				},
				error: function(xhr, err)
				{
					$('.loading-indication-mini').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function delete_search_mail_quarantine(token, selected_quarantine_email, dom_id)
{
	var email_checked = [];
	var join_email_selected;

	$(".email-check:checked").each(function() {
		email_checked.push($(this).val());
	});

	join_email_selected = email_checked.join(', ') ;

	if(join_email_selected.length > 0)
	{
		var dataString = {'delete_search_mail_quarantine' : 'true', 'selected_quarantine_email' : selected_quarantine_email, 'dom_id' : dom_id, 'email_list' : join_email_selected, 'token' : token};
	}

	else
	{
		var dataString = {'delete_search_mail_quarantine' : 'true', 'domain_list' : 'null', 'token' : token};
	}

		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine_search.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalDisableDomain").html(data);
					$('#ModalDisableDomain').modal('show');
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function execute_delete_search_mail_quarantine(token, email_list)
{
	var dataString = {'execute_delete_search_mail_quarantine' : 'true', 'email_list' : email_list, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
			type: "POST",
			url: "mail_quarantine_search.php",
			data: dataString,
			cache: false,
			success: function(data)
			{
				$('.loading-indication').remove();
				$("#ResponseModalDisableDomain").html(data);
			},
			error: function(xhr, err)
			{
				$('.loading-indication').remove();
				get_ajax_error(xhr, err);
			}
	});
}

function release_search_mail_quarantine(token, selected_quarantine_email, dom_id)
{
	var email_checked = [];
	var join_email_selected;

	$(".email-check:checked").each(function() {
		email_checked.push($(this).val());
	});

	join_email_selected = email_checked.join(', ') ;

	if(join_email_selected.length > 0)
	{
		var dataString = {'release_search_mail_quarantine' : 'release_search_mail_quarantine', 'selected_quarantine_email' : selected_quarantine_email, 'dom_id' : dom_id, 'email_list' : join_email_selected, 'token' : token};
	}

	else
	{
		var dataString = {'release_search_mail_quarantine' : 'release_search_mail_quarantine', 'domain_list' : 'null', 'token' : token};
	}

		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine_search.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalDisableDomain").html(data);
					$('#ModalDisableDomain').modal('show');
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function execute_release_search_mail_quarantine(token, email_list, dom_name, dom_id)
{
	var dataString = {'execute_release_search_mail_quarantine' : 'execute_release_search_mail_quarantine', 'email_list' : email_list, 'dom_name' : dom_name, 'dom_id' : dom_id, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
			type: "POST",
			url: "mail_quarantine_search.php",
			data: dataString,
			cache: false,
			success: function(data)
			{
				$('.loading-indication').remove();
				$("#ResponseModalDisableDomain").html(data);
			},
			error: function(xhr, err)
			{
				$('.loading-indication').remove();
				get_ajax_error(xhr, err);
			}
	});
}

function add_domain_search_quarantine_to_whitelist(token, selected_quarantine_email, dom_id)
{
	var email_checked = [];
	var join_email_selected;

	$(".email-check:checked").each(function() {
		email_checked.push($(this).val());
	});

	join_email_selected = email_checked.join(', ') ;

	if(join_email_selected.length > 0)
	{
		var dataString = {'add_domain_search_quarantine_to_whitelist' : 'true', 'selected_quarantine_email' : selected_quarantine_email, 'dom_id' : dom_id, 'domain_list' : join_email_selected, 'token' : token};
	}

	else
	{
		var dataString = {'add_domain_search_quarantine_to_whitelist' : 'true', 'domain_list' : 'null', 'token' : token};
	}

		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine_search.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalDisableDomain").html(data);
					$('#ModalDisableDomain').modal('show');
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function execute_add_domain_search_quarantine_to_whitelist(token, domain_list, dom_id)
{
	$(".select-all").prop('checked', false);

	var dataString = {'execute_add_domain_search_quarantine_to_whitelist' : 'true', 'domain_list' : domain_list, "dom_id" : dom_id, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
			type: "POST",
			url: "mail_quarantine_search.php",
			data: dataString,
			cache: false,
			success: function(data)
			{
				$('.loading-indication').remove();
				$("#ResponseModalDisableDomain").html(data);
			},
			error: function(xhr, err)
			{
				$('.loading-indication').remove();
				get_ajax_error(xhr, err);
			}
	});
}

function view_search_mail_quarantine(token, msg_id)
{
	if(msg_id)
	{
		var dataString = {'view_search_mail_quarantine' : 'view_search_mail_quarantine', 'msg_id' : msg_id, 'token' : token};
	}
	else
	{
		var dataString = {'view_search_mail_quarantine' : 'view_search_mail_quarantine', 'msg_id' : 'null', 'token' : token};
	}

		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine_search.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalDisableDomain").html(data);
					$('#ModalDisableDomain').modal('show');
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function add_email_search_quarantine_to_whitelist(token, selected_quarantine_email, dom_id)
{
	var email_checked = [];
	var join_email_selected;

	$(".email-check:checked").each(function() {
		email_checked.push($(this).val());
	});

	join_email_selected = email_checked.join(', ') ;

	if(join_email_selected.length > 0)
	{
		var dataString = {'add_email_search_quarantine_to_whitelist' : 'true', 'selected_quarantine_email' : selected_quarantine_email, 'dom_id' : dom_id, 'email_list' : join_email_selected, 'token' : token};
	}

	else
	{
		var dataString = {'add_email_search_quarantine_to_whitelist' : 'true', 'email_list' : 'null', 'token' : token};
	}

		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
				type: "POST",
				url: "mail_quarantine_search.php",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$('.loading-indication').remove();
					$("#ResponseModalDisableDomain").html(data);
					$('#ModalDisableDomain').modal('show');
				},
				error: function(xhr, err)
				{
					$('.loading-indication').remove();
					get_ajax_error(xhr, err);
				}
		});
}

function execute_add_email_search_quarantine_to_whitelist(token, email_list, dom_id)
{
	$(".select-all").prop('checked', false);

	var dataString = {'execute_add_email_search_quarantine_to_whitelist' : 'true', 'email_list' : email_list, "dom_id" : dom_id, 'token' : token};

	$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
			type: "POST",
			url: "mail_quarantine_search.php",
			data: dataString,
			cache: false,
			success: function(data)
			{
				$('.loading-indication').remove();
				$("#ResponseModalDisableDomain").html(data);
			},
			error: function(xhr, err)
			{
				$('.loading-indication').remove();
				get_ajax_error(xhr, err);
			}
	});
}

/*===================================mail_report_traffic_daily.php======================================*/
function select_domain_daily_traffic_report(token, sortby = "")
{
	var select_domain_traffic_report = $('.select_domain_daily_report').val();
	var dataString = {'select_domain_traffic_report' : select_domain_traffic_report, 'start_date' : $('#start_date').val(), 'sortby' : sortby, 'token' : token};

	if (select_domain_traffic_report != 'undefined' && select_domain_traffic_report != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
			type: "POST",
			url: "mail_daily_traffic_report.php",
			cache: true,
			data: dataString,
			cache: false,
			success: function(data)
			{
				$('.loading-indication').remove();
				$(".traffic-content").html(data);
			},
			error: function(xhr, err)
			{
				$('.loading-indication').remove();
				get_ajax_error(xhr, err);
			}
		});
  }
}

function select_domain_traffic_report(token)
{
	var select_domain_traffic_report = $('.select_domain_daily_report').val();
	var range = $('#range').val();

	if (range.toLowerCase() == 'custom' && $('#start_date').val() != '' && $('#end_date').val() != '')
	{
		var dataString = {'select_domain_traffic_report' : select_domain_traffic_report, 'range' : range, 'start_date' : $('#start_date').val(), 'end_date' : $('#end_date').val(), 'token' : token};
	}
	else
	{
		var dataString = {'select_domain_traffic_report' : select_domain_traffic_report, 'range' : range, 'token' : token};
	}

	if (select_domain_spam_report != 'undefined' && select_domain_traffic_report != '' && range != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_traffic_report.php",
					cache: true,
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".traffic-content").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_domain_spam_report(token)
{
	var select_domain_spam_report = $('.select_domain_daily_report').val();
	var range = $('#range').val();

	if (range.toLowerCase() == 'custom' && $('#start_date').val() != '' && $('#end_date').val() != '')
	{
		var dataString = {'select_domain_spam_report' : select_domain_spam_report, 'range' : range, 'start_date' : $('#start_date').val(), 'end_date' : $('#end_date').val(), 'token' : token};
	}
	else
	{
		var dataString = {'select_domain_spam_report' : select_domain_spam_report, 'range' : range, 'token' : token};
	}

	if (select_domain_spam_report != 'undefined' && select_domain_spam_report != '' && range != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_spam_report.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".traffic-content").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_domain_virus_report(token)
{
	var select_domain_virus_report = $('.select_domain_daily_report').val();
	var range = $('#range').val();

	if (range.toLowerCase() == 'custom' && $('#start_date').val() != '' && $('#end_date').val() != '')
	{
		var dataString = {'select_domain_virus_report' : select_domain_virus_report, 'range' : range, 'start_date' : $('#start_date').val(), 'end_date' : $('#end_date').val(), 'token' : token};
	}
	else
	{
		var dataString = {'select_domain_virus_report' : select_domain_virus_report, 'range' : range, 'token' : token};
	}

	if (select_domain_virus_report != 'undefined' && select_domain_virus_report != '' && range != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_virus_report.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".traffic-content").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_domain_daily_report(token)
{
	$('div.wrapper_date_domain_daily_report').show();

	var select_domain_daily_report = $('.select_domain_daily_report').val();
	var date_domain_daily_report = $('.date_domain_daily_report').val()

	var dataString = {'select_domain_daily_report' : select_domain_daily_report, 'date_domain_daily_report' : date_domain_daily_report, 'token' : token};

	if (select_domain_daily_report != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_report_traffic_daily.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".traffic-content").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_domain_weekly_report(token)
{
	var select_domain_weekly_report = $('.select_domain_weekly_report').val();

	var dataString = {'select_domain_weekly_report' : select_domain_weekly_report, 'token' : token};

	if (select_domain_weekly_report != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_report_traffic_week.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".traffic-content").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_domain_monthly_report(token)
{
	$('div.wrapper_date_domain_monthly_report').show();

	var date_domain_monthly_report = $('.date_domain_monthly_report').val()
	var select_domain_monthly_report = $('.select_domain_monthly_report').val();

	var dataString = {'select_domain_monthly_report' : select_domain_monthly_report, 'date_domain_monthly_report' : date_domain_monthly_report, 'token' : token};

	if (select_domain_monthly_report != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_report_traffic_monthly.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".traffic-content").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function protected_report(domain, token)
{
	$('div.wrapper_date_domain_monthly_report').show();

	var date_domain_monthly_report = $('.date_domain_monthly_report').val()

	var dataString = {'select_domain_monthly_report' : domain, 'date_domain_monthly_report' : date_domain_monthly_report, 'token' : token};

	if (select_domain_monthly_report != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "protected_mail_report.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".traffic-content").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}


/*======================================MONGO REPORT=======================================*/

function select_domain_traffic_report_htio(token)
{
	var select_domain_traffic_report = $('.select_domain_daily_report').val();
	var range = $('#range').val();

	if (range.toLowerCase() == 'custom' && $('#start_date').val() != '' && $('#end_date').val() != '')
	{
		var dataString = {'select_domain_traffic_report' : select_domain_traffic_report, 'range' : range, 'start_date' : $('#start_date').val(), 'end_date' : $('#end_date').val(), 'token' : token};
	}
	else
	{
		var dataString = {'select_domain_traffic_report' : select_domain_traffic_report, 'range' : range, 'token' : token};
	}

	if (select_domain_spam_report != 'undefined' && select_domain_traffic_report != '' && range != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_traffic_report_htio.php",
					cache: true,
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".traffic-content").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_domain_spam_report_htio(token)
{
	var select_domain_spam_report = $('.select_domain_daily_report').val();
	var range = $('#range').val();

	if (range.toLowerCase() == 'custom' && $('#start_date').val() != '' && $('#end_date').val() != '')
	{
		var dataString = {'select_domain_spam_report' : select_domain_spam_report, 'range' : range, 'start_date' : $('#start_date').val(), 'end_date' : $('#end_date').val(), 'token' : token};
	}
	else
	{
		var dataString = {'select_domain_spam_report' : select_domain_spam_report, 'range' : range, 'token' : token};
	}

	if (select_domain_spam_report != 'undefined' && select_domain_spam_report != '' && range != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_spam_report_htio.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".traffic-content").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_domain_virus_report_htio(token)
{
	var select_domain_virus_report = $('.select_domain_daily_report').val();
	var range = $('#range').val();

	if (range.toLowerCase() == 'custom' && $('#start_date').val() != '' && $('#end_date').val() != '')
	{
		var dataString = {'select_domain_virus_report' : select_domain_virus_report, 'range' : range, 'start_date' : $('#start_date').val(), 'end_date' : $('#end_date').val(), 'token' : token};
	}
	else
	{
		var dataString = {'select_domain_virus_report' : select_domain_virus_report, 'range' : range, 'token' : token};
	}

	if (select_domain_virus_report != 'undefined' && select_domain_virus_report != '' && range != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
	        $.ajax({
					type: "POST",
					url: "mail_virus_report_htio.php",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('.loading-indication').remove();
						$(".traffic-content").html(data);
					},
					error: function(xhr, err)
					{
						$('.loading-indication').remove();
						get_ajax_error(xhr, err);
					}
			});
	    }
}

function select_domain_daily_traffic_report_htio(token, sortby = "")
{
	var select_domain_traffic_report = $('.select_domain_daily_report').val();
	var dataString = {'select_domain_traffic_report' : select_domain_traffic_report, 'start_date' : $('#start_date').val(), 'sortby' : sortby, 'token' : token};

	if (select_domain_traffic_report != 'undefined' && select_domain_traffic_report != '')
	{
		$('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
    $.ajax({
			type: "POST",
			url: "mail_daily_traffic_report_htio.php",
			cache: true,
			data: dataString,
			cache: false,
			success: function(data)
			{
				$('.loading-indication').remove();
				$(".traffic-content").html(data);
			},
			error: function(xhr, err)
			{
				$('.loading-indication').remove();
				get_ajax_error(xhr, err);
			}
		});
  }
}