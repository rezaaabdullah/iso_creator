<?php
include_once "config.php";

function deleteDirectory($dir) 
{
	if (!file_exists($dir)) 
	{
		return true;
	}

	if (!is_dir($dir)) 
	{
		return unlink($dir);
	}

	foreach (scandir($dir) as $item) 
	{
		if ($item == '.' || $item == '..') 
		{
			continue;
		}

		if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) 
		{
			return false;
		}
	}

	return rmdir($dir);
}

function recurse_copy($src,$dst) { 
	$dir = opendir($src); 
	if(!is_dir($dst)) mkdir($dst); 

	while(false !== ( $file = readdir($dir)) ) { 
			if (( $file != '.' ) && ( $file != '..' )) { 
					if ( is_dir($src . '/' . $file) ) { 
							recurse_copy($src . '/' . $file,$dst . '/' . $file); 
					} 
					else { 
							copy($src . '/' . $file,$dst . '/' . $file); 
					} 
			} 
	} 
	closedir($dir); 
} 

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{ 
	if(isset($_POST["get_update_list"]))
  { 
		$startTime = microtime(true);
		$download_list = array();
		$undefined_installer_url_list = array();
		$unknown_product_list = array();

		if($_POST["data"] != "") $data = explode("#", $_POST["data"]);
    else $data = array();

    if(count($data) > 0)
    {
			foreach ($data as $value) 
      {
				$value = rtrim($value);

        if(isset($iso_component_list[$value]))
        {
					if(isset($iso_component_list[$value]["installer"]) && 
						count($iso_component_list[$value]["installer"]) > 0)
					{
						foreach ($iso_component_list[$value]["installer"] as $installer) 
						{
							$download_list[] = ["dir" => $installer["source_dir"], "url" => $installer["url_download"]];
						}						
					}
					else
					{
						if(!in_array($value, $undefined_installer_url_list)) $undefined_installer_url_list[]  = ["filename" => $value];
					}
				}
				else
				{
					if(!in_array($value, $unknown_product_list)) $unknown_product_list[] = ["filename" => $value];
				}				
			}
			
			if(count($unknown_product_list) > 0)
			{
				$return["log"] = "update_101";
				$return["errdesc"] = "Oops! Kode product nggak valid.";
				$return["list"] = $unknown_product_list;
			}
			else if(count($undefined_installer_url_list) > 0)
			{
				$return["log"] = "update_102";
				$return["errdesc"] = "Oops! Ada data path yang nggak ketemu, silakan tambah di file config.php.";
				$return["list"] = $undefined_installer_url_list;
			}
			else if(count($download_list) > 0)
			{
				$return["log"] = "0";
				$return["errdesc"] = "sukses";
				$return["list"] = $download_list;
			}
		}
    else
    {
			$return["log"] = "update_100";
			$return["errdesc"] = "Oops! Silahkan pilih produknya dulu ya.";
		}
		
    $return["execution_time"] = number_format(( microtime(true) - $startTime), 4);

    echo json_encode($return);
	}

  elseif(isset($_POST["check_all_requirement"]))
  {	
		$startTime = microtime(true);
		$unknown_product_list = array();
		$undefined_installer_url_list = array();
		$undefined_installer_url_download_list = array();
		$downloaded_list = array();
		$undownloaded_list = array();
    $return = array();

    if($_POST["data"] != "") $data = explode("#", $_POST["data"]);
    else $data = array();

    if(count($data) > 0)
    {
      foreach ($data as $value) 
      {
				$value = rtrim($value);

        if(isset($iso_component_list[$value]))
        {
					if(isset($iso_component_list[$value]["installer"]) && 
						count($iso_component_list[$value]["installer"]) > 0)
					{
						foreach ($iso_component_list[$value]["installer"] as $installer) 
						{
							if(!is_file($installer["source_dir"]))
							{
								if(isset($installer["url_download"]) && $installer["url_download"] != "")
								{
									$pos = strrpos($installer["url_download"], "/");
									$filename = substr($installer["url_download"], $pos + 1);
		
									$undownloaded_list[] = ["filename" => $filename, "data" => ["url" => $installer["url_download"], "dir" => $installer["source_dir"]]];
								}
								else
								{
									if(count($undefined_installer_url_download_list) > 0)
									{
										$exist = false;
	
										foreach ($undefined_installer_url_download_list as $val_data) 
										{
											if($val_data["filename"] == $value)
											{
												$exist = true;
											}
										}
									}
									else
									{
										$exist = false;
									}

									if(!$exist) $undefined_installer_url_download_list[] = ["filename" => $value];
								}
							}
							else
							{
								$downloaded_list[] = ["filename" => basename($installer["source_dir"]), 
																			"dir" => $installer["source_dir"], 
																			"last_modify" => filemtime($installer["source_dir"]),
																			"url" => $installer["url_download"]];
							}
						}						
					}
					else
					{
						if(!in_array($value, $undefined_installer_url_list)) $undefined_installer_url_list[]  = ["filename" => $value];
					}
				}
				else
				{
					if(!in_array($value, $unknown_product_list)) $unknown_product_list[] = ["filename" => $value];
				}				
			}
			
			if(count($unknown_product_list) > 0)
			{
				$return["log"] = "check_101";
				$return["errdesc"] = "Oops! Kode product nggak valid.";
				$return["list"] = $unknown_product_list;
			}
			else if(count($undefined_installer_url_list) > 0)
			{
				$return["log"] = "check_102";
				$return["errdesc"] = "Oops! Ada data path yang nggak ketemu, silakan tambah di file config.php.";
				$return["list"] = $undefined_installer_url_list;
			}
			else if(count($undefined_installer_url_download_list) > 0)
			{
				$return["log"] = "check_103";
				$return["errdesc"] = "Oops! Url download installer nggak ketemu, silakan tambah url di file config.php.";
				$return["list"] = $undefined_installer_url_download_list;
			}
			else if(count($undownloaded_list) > 0)
			{
				$return["log"] = "check_104";
				$return["errdesc"] = "Oops! Ada file yang belum terunduh nih.";
				$return["list"] = $undownloaded_list;
			}
			else
			{
				$return["log"] = "0";
				$return["errdesc"] = "sukses";
				$return["list"] = $downloaded_list;
			}
    }
    else
    {
			$return["log"] = "check_100";
			$return["errdesc"] = "Oops! Silahkan pilih produknya dulu ya.";
		}
		
    $return["execution_time"] = number_format(( microtime(true) - $startTime), 4);

    echo json_encode($return);
	}
	
	else if(isset($_POST["download_installer"]))
  {
		$startTime = microtime(true);
		$unknown_dir = array();
		$unwritable_dir = array();
		$success_download = array();
		$failed_download = array();
		$return = array();
		
		//tambah proses download file yang punya value url_download

		if(isset($_POST["data"]) && count($_POST["data"]) > 0)
		{
			foreach ($_POST["data"] as $data) 
			{
				$pos = strrpos($data["dir"], "/");
				$dir = substr($data["dir"], 0, $pos + 1);
				$current_pos = strpos($dir, "/INSTALLER/");
				$current_dir = substr($dir, $current_pos + strlen("/INSTALLER/"));
				$main_dir 	 = substr($dir, 0, $current_pos + strlen("/INSTALLER/"));
				$filename = substr($data["dir"], $pos + 1);

				if(!is_dir($dir))
				{
					$dir_exp = explode("/", $current_dir);

					for ($i=0; $i < count($dir_exp); $i++) 
					{ 
						if($i == count($dir_exp) -1) break;
						if($i > 0) $dir_exp[$i] = $dir_exp[$i - 1] ."/". $dir_exp[$i];
						if(!is_dir($main_dir .$dir_exp[$i] ."/"))
						{
							mkdir($main_dir .$dir_exp[$i] ."/", 0755);
						}
					}
				}

				if(is_writable($dir))
				{
					$ch = curl_init();
					$fp = fopen($data["dir"], 'wb');
					curl_setopt($ch, CURLOPT_URL, $data["url"]);
					curl_setopt($ch, CURLOPT_FILE, $fp);
					$run = curl_exec($ch);
					$http_response_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
					curl_close($ch);
					fclose($fp);

					if($http_response_code == "200")
					{
						$success_download[] = $filename;
					}
					else
					{
						$failed_download[] = $filename;
					}
				}
				else
				{
					$unwritable_dir[] = $dir;
				}
			}

			if(count($unknown_dir) > 0)
			{
				$return["log"] = "download_101";
				$return["errdesc"] = "Oops! Directory untuk simpan file download nggak ketemu.";
				$return["list"] = ["failed" => $unknown_dir];
			}
			elseif(count($unwritable_dir) > 0)
			{
				$return["log"] = "download_102";
				$return["errdesc"] = "Oops! Nggak ada akses tulis file di directory untuk simpan download.";
				$return["list"] = ["failed" => $unknown_dir];
			}
			else
			{
				$return["log"] = (count($failed_download) > 0) ? "download_103" : "0";
				$return["errdesc"] = (count($failed_download) > 0) ? "Oops! Beberapa file gagal diunduh." : "sukses";

				if(count($success_download) > 0) $return["list"]["success"] = $success_download;
				if(count($failed_download) > 0) $return["list"]["failed"] = $failed_download;
			}
		}
		else
		{
			$return["log"] = "download_100";
			$return["errdesc"] = "Oops! Data produk yang mau didownload kosong.";	
		}

		$return["execution_time"] = number_format(( microtime(true) - $startTime), 4);

    echo json_encode($return);    
	}
	
	else if(isset($_POST["create_iso"]))
	{
		$startTime = microtime(true);
		$unknown_product_list = array();
		$not_exist_file = array();
		$fail_copy = array();
		$success_convert = array();
		$failed_convert = array();

		if($_POST["data"] != "") $data = explode("#", $_POST["data"]);
		else $data = array();
		
		if(count($data) > 0)
    {
			if(is_writable($iso_component_dir))
			{
				foreach ($data as $value) 
				{
					$fail_copy_temp = array();

					if(isset($iso_component_list[$value]))
					{
						if(is_dir($iso_component_dir .$value ."/"))
						{
							deleteDirectory($iso_component_dir .$value ."/");
						}
																	
						foreach ($iso_component_list[$value] as $component_key => $component)
						{
							if(is_array($component) && count($component) > 0)
							{
								foreach ($component as $installer) 
								{

									if(is_dir($installer["source_dir"]))
									{
										$pos_main_dir = strpos($installer["destination_dir"], "/ISO_BLUEPRINT/");
										$get_main_dir = substr($installer["destination_dir"], 0, $pos_main_dir + strlen("/ISO_BLUEPRINT/"));
										$get_dest_dir = substr($installer["destination_dir"], $pos_main_dir + strlen("/ISO_BLUEPRINT/"));

										recurse_copy($installer["source_dir"], $get_main_dir .$value ."/". $get_dest_dir);
									}
									elseif(is_file($installer["source_dir"]))
									{
										$pos = strrpos($installer["source_dir"], "/");
										$filename = substr($installer["source_dir"], $pos + 1);

										$pos_main_dir = strpos($installer["destination_dir"], "/ISO_BLUEPRINT/");
										$get_main_dir = substr($installer["destination_dir"], 0, $pos_main_dir + strlen("/ISO_BLUEPRINT/"));
										$get_dest_dir = substr($installer["destination_dir"], $pos_main_dir + strlen("/ISO_BLUEPRINT/"));
										$get_dest_dir_ori = $get_dest_dir;
										$get_dest_dir = explode("/", $get_dest_dir);
		
										if(!is_dir($get_main_dir ."/". $value ."/"))
										{
											mkdir($get_main_dir ."/". $value ."/", 0755);
										}
		
										for ($i=0; $i < count($get_dest_dir); $i++) 
										{ 
											if($i == count($get_dest_dir) -1) break;
											if($i > 0) $get_dest_dir[$i] = $get_dest_dir[$i - 1] ."/". $get_dest_dir[$i];
											if(!is_dir($get_main_dir ."/". $value ."/". $get_dest_dir[$i] ."/"))
											{
												mkdir($get_main_dir ."/". $value ."/". $get_dest_dir[$i] ."/", 0755);
											}
										}
		
										if(!copy($installer["source_dir"], $get_main_dir .$value ."/". $get_dest_dir_ori))
										{
											$fail_copy[] = $fail_copy_temp[] = $filename;
										}
									}
									else
									{
										$not_exist_file[] = $installer["source_dir"] ." <b>(". $component_key .")</b>";
									}
								}
							}
						}
						
						if(count($fail_copy_temp) < 1 && count($not_exist_file) < 1)
						{
							$create_iso = shell_exec("genisoimage -R -J -v -o ". $iso_result_dir.$value ."_". date("Y_m_d_H_i_s") .".iso ". $iso_component_dir .$value ." 2>&1");

							preg_match_all("/(^[0-9].* extents written \([0-9].*\s[\w].*\)$)/m", $create_iso, $create_iso_status, PREG_SET_ORDER, 0);

							if(isset($create_iso_status[0][0]))
							{
								$success_convert[] = $value;
							}
							else
							{
								$failed_convert[] = $value;
							}
						}
					}
					else
					{
						$unknown_product_list[] = $value;
					}
				}

				if(count($unknown_product_list) > 0)
				{
					$return["log"] = "create_iso_102";
					$return["errdesc"] = "Oops! Terdapat produk yang tidak valid, produk berikut gagal diproses.";
					$return["list"] = ["failed" => $unknown_product_list];
				}
				elseif(count($fail_copy) > 0)
				{
					$return["log"] = "create_iso_103";
					$return["errdesc"] = "Oops! Terjadi kesalahan saat copy ke iso blueprint, produk berikut gagal diproses.";
					$return["list"] = ["failed" => $fail_copy];
				}
				elseif(count($not_exist_file) > 0)
				{
					$return["log"] = "create_iso_104";
					$return["errdesc"] = "Oops! File installer tidak ditemukan, convert ISO gagal diproses.";
					$return["list"] = ["failed" => $not_exist_file];
				}
				else
				{
					$return["log"] = (count($failed_convert) > 0) ? "create_iso_105" : "0";
					$return["errdesc"] = (count($failed_convert) > 0) ? "Oops! Beberapa file gagal diconvert." : "sukses";

					if(count($success_convert) > 0) $return["list"]["success"] = $success_convert;
					if(count($failed_convert) > 0) $return["list"]["failed"] = $failed_convert;
				}
			}
			else
			{
				$return["log"] = "create_iso_101";
				$return["errdesc"] = "Oops! Nggak ada akses tulis file di directory iso blueprint.";
			}
		}
		else
		{
			$return["log"] = "create_iso_100";
			$return["errdesc"] = "Oops! Silahkan pilih produknya dulu ya.";
		}

		$return["execution_time"] = number_format(( microtime(true) - $startTime), 4);

		echo json_encode($return);
	}
}
?>