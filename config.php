<?php

$storage_dir									= __DIR__. "/../iso_creator_data/";
$manual_dir										= $storage_dir ."MANUAL/";
$installer_endpoint_dir				= $storage_dir ."INSTALLER/ENDPOINT/";
$installer_ova_appliance_dir	= $storage_dir ."INSTALLER/APPLIANCE/";
$installer_eracomp_dir				= $storage_dir ."INSTALLER/ERA/STANDALONE/";
$installer_all_in_one_dir			= $storage_dir ."INSTALLER/ERA/ALLINONE/";
$iso_component_dir						= $storage_dir ."ISO_BLUEPRINT/";
$iso_result_dir								= $storage_dir ."ISO_RESULT/";
$config_iso_dir 							= __DIR__ ."/config_iso/";

$config_iso_list = array_diff(scandir($config_iso_dir), array('..', '.'));

$iso_component_list = array();

if(count($config_iso_list) > 0)
{	
	foreach ($config_iso_list as $name) 
	{
		$name = explode(".", $name);

		if(isset($name[1]) && $name[1] == "php")
		{
			include_once $config_iso_dir. $name[0]. ".php";
			$iso_component_list[$name[0]] = ${$name[0]};			
		}
	}
}

?>