<?php
include_once "config.php";

if(isset($_GET["go"]) && $_GET["go"] == "download")
{
	$file = (isset($_GET["data"])) ? $iso_result_dir .$_GET["data"] : "";
	$filename = basename($file);

	header("Cache-Control: public");
	header("Content-Description: File Transfer");
	header("Content-Disposition: attachment; filename=$filename");
	header("Content-Type: application/octet-stream");
	header("Content-Transfer-Encoding: binary");

	readfile($file);
}
elseif(isset($_GET["go"]) && $_GET["go"] == "delete")
{
	$file = (isset($_GET["data"])) ? $iso_result_dir .$_GET["data"] : "";

	if(is_file($file))
	{
		$hapus = unlink($file);
		$file = basename($file);
		
		if($hapus)
		{
		?>
		<script>
			location.href = "index.php?msg=sukses";
		</script>
		<?php
		}
		else
		{
		?>
		<script>
			location.href = "index.php?msg=File <?php echo $file; ?> gagal dihapus.";
		</script>
		<?php
		}
	}
}
?>