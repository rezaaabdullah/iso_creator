<?php
include_once "config.php";

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
  if(isset($_POST["load_iso_list"]))
  {
    if(isset($iso_result_dir))
    {
      $file_list = array_diff(scandir($iso_result_dir), array('..', '.'));

      if(count($file_list) > 0)
      {
        $dump_file_list = array();
  
        foreach ($file_list as $file) 
        {
          $dump_file_list[] = ["filename" => $file, "last_modified" => filemtime($iso_result_dir.$file)];
        }

        $sorted_dump_file_list = array_column($dump_file_list, 'last_modified');
        array_multisort($sorted_dump_file_list, SORT_DESC, $dump_file_list);        
  
        foreach ($dump_file_list as $file) 
        {
        ?>
        <tr>
          <td>
            <?php echo $file["filename"]; ?>
          </td>
  
          <td>
            <?php echo date("Y F j H:i:s", $file["last_modified"]); ?>
          </td>
  
          <td style="text-align: center;">
            <a target="_blank" href="manage_iso.php?go=download&data=<?php echo $file["filename"]; ?>" rel="tooltip" data-placement="bottom" title="Download ISO"  class="btn btn-primary">
              <i class="fas fa-download"></i>
            </a>
  
            <a href="manage_iso.php?go=delete&data=<?php echo $file["filename"]; ?>" rel="tooltip" data-placement="bottom" title="Delete ISO"  class="btn btn-danger">
              <i class="fas fa-trash-alt"></i>
            </a>
          </td>
        </tr>
        <?php
        }
      }
      else
      {
      ?>
        <tr><td colspan="3"><i><center>Data not found</center></i></td></tr>
        <?php
      }
    }
  }
}
else
{
?>
<html>
	<head>
    <title>Create ISO</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html" />

    <script src="assets/jquery-3.3.1.js"></script>

    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
    <script src="assets/bootstrap/js/bootstrap.js"></script>

    <link rel="stylesheet" href="assets/fontawesome/css/fontawesome-all.min.css">
    <script>
    window.FontAwesomeConfig = {searchPseudoElements: true}
    </script>

    <script src="assets/js/library.js"></script>

    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
	</head>
	<body onload="load_iso_list()">
    <div class="col-xs-12 content-header">
      <div class="col-xs-12 content-wrapper">
          <h1 class="title">ESET BUSINESS ISO</h1>
      </div>
    </div>

    <div class="col-xs-12 content-wrapper">
			<div class="col-xs12 col-sm-6">
				<div class="col-xs-12 table-responsive content-body">
					<table class="table table-striped">
            <tr>
              <th>
              <div class="col-xs-6" style="padding: 0;">
                <span style="position: relative; top: 4px;">PRODUCT LIST</span>
              </div>

              <div class="col-xs-6" style="padding:0; text-align: right;">
                <button data-toggle="modal" rel="tooltip" data-placement="bottom" title="Update Installer" class="btn btn-primary btn-update">
                  <i class="fas fa-sync-alt"></i>
                </button>

                <button data-toggle="modal" rel="tooltip" data-placement="bottom" title="Create ISO" class="btn btn-primary btn-download">
                  <i class="fas fa-paper-plane"></i>
                </button>
                
                <label class="switch" data-toggle="tooltip" title="Select All" data-placement="bottom">
                  <input type="checkbox" class="select-all">
                  <span class="slider round"></span>
                </label>
              </div>
              </th>
            </tr>
						<?php
						if(isset($iso_component_list) && count($iso_component_list) > 0)
						{
							foreach ($iso_component_list as $key => $value) 
							{
							?>
							<tr>
							<td style="width: 80%">
								<label class="select-installer">
									<?php echo $value["product_name"]; ?>
									<label class="select" data-toggle="tooltip" title="" data-placement="auto" data-original-title="Select" style="float: right;">
										<input type="checkbox" class="selected_installer" value="<?php echo $key; ?>">
										<span class="fas fa-check"></span>
									</label>
								</label>
							</td>
							</tr>
							<?php				
							}
            }            
						?>            
					</table>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="col-xs-12 table-responsive content-body">
          <?php
          if(isset($_GET["msg"]))
          {  
            if($_GET["msg"] == "sukses")
            {
            ?>
            <div class="alert alert-success alert-dismissible">
              <a href="index.php" class="close" aria-label="close">&times;</a>
              <strong>Sukses!</strong> File ISO berhasil dihapus.
            </div>
            <?php
            }
            else
            {
            ?>
            <div class="alert alert-danger alert-dismissible">
              <a href="index.php" class="close" aria-label="close">&times;</a>
              <strong>Terjadi kesalahan!</strong> <?php echo $_GET["msg"]; ?>
            </div>
            <?php
            }
          }
          ?>

					<table class="table table-striped table-list-iso">
					<tr>
						<th>Filename</th>
						<th>Last Modified</th>
						<th>Action</th>
					</tr>
          
          <tbody id="ResponseLoadIso"></tbody>
					</table>
				</div>
      </div>
    </div>

    <div class="modal fade" id="ModalUpdate" data-keyboard="false" data-backdrop="static" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content" style="padding: 50px 20px;">
          <h1 class="notif-title">LOREM IPSUM!</h1>

          <i class="notif-icon failed fas fa-times"></i>

          <div class="notif-desc">
            Modal Update
          </div>

          <button data-dismiss="modal" class="btn notif-btn failed">SELESAI</button>
        </div>
      </div>
    </div>

    <div class="modal fade" id="ModalCheck" data-keyboard="false" data-backdrop="static" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content" style="padding: 50px 20px;">
          <h1 class="notif-title">LOREM IPSUM!</h1>

          <i class="notif-icon failed fas fa-times"></i>

          <div class="notif-desc">
            Modal Check
          </div>

          <button data-dismiss="modal" class="btn notif-btn failed">SELESAI</button>
        </div>
      </div>
    </div>

    <div class="modal fade" id="ModalProcess" data-keyboard="false" data-backdrop="static" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content" style="padding: 50px 20px;">
          <h1 class="notif-title">LOREM IPSUM!</h1>

          <i class="notif-icon failed fas fa-times"></i>

          <div class="notif-desc">
            Modal Process
          </div>

          <button data-dismiss="modal" class="btn notif-btn failed">ULANGI PROSES</button>
        </div>
      </div>
    </div>

    <div class="modal fade" id="ModalResult" data-keyboard="false" data-backdrop="static" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content" style="padding: 50px 20px;">
          <h1 class="notif-title">LOREM IPSUM!</h1>

          <i class="notif-icon failed fas fa-times"></i>

          <div class="notif-desc">
            Modal Result
          </div>

          <button data-dismiss="modal" class="btn notif-btn failed">SELESAI</button>
        </div>
      </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function () 
    {
      $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip(
        {
        container:'body', trigger: 'hover'
      });

      $(".select-all").click(function ()
      {
        $('input:checkbox').prop('checked', this.checked);
      });

      $("input:checkbox").not('.select-all').click(function ()
      {
        if ($('input:checkbox:checked').not('.select-all').length == $('input:checkbox').not('.select-all').length)
        {
          $('.select-all').prop('checked', true);
        }
        else
        {
          $('.select-all').prop('checked', false);
        }
      });

      $(".btn-update").click(function()
      {
        var installer = [];
        var join_installer;

        $(".selected_installer:checked").each(function() 
        {
          installer.push($(this).val());
        });

        join_installer = installer.join('#') ;

        $('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
          type: "POST",
          url: "create_iso.php",
          data: {"get_update_list" : true, "data" : join_installer},
          cache: false,
          success: function(data)
          {             
            var data = JSON.parse(data);
            console.log(data);

            $('.loading-indication').remove();

            if(data.log == "0")
            {
              var download = [];
              
              if(typeof data.list !== 'undefined')
              {
                for (let i = 0; i < data.list.length; i++) 
                {
                  download.push(data.list[i]);
                }
              }

              download_file(download, "just_update");
            }
            else
            {
              var message = data.errdesc;
              
              if(typeof data.list !== 'undefined')
              {
                message += " <br> List: ";

                for (let i = 0; i < data.list.length; i++) 
                {
                  message += data.list[i]["filename"] + ", "; 
                }

                message = message.substr(0, (message.length - 2));
              }

              $("#ModalUpdate .modal-content > .notif-title").html("TERJADI KESALAHAN!");
              $("#ModalUpdate .modal-content > .notif-icon").removeClass("failed fas fas-times fa-check");
              $("#ModalUpdate .modal-content > .notif-icon").addClass("failed fas fa-times");
              $("#ModalUpdate .modal-content > .notif-desc").html(message);
              $("#ModalUpdate .modal-content > .notif-btn").removeClass("failed");
              $("#ModalUpdate .modal-content > .notif-btn").addClass("failed");
              $('#ModalUpdate').modal('show');
            }

          },
          error: function(xhr, err)
          {
            $('.loading-indication').remove();
            $("#ModalUpdate .modal-content > .notif-title").html("TERJADI KESALAHAN!");
            $("#ModalUpdate .modal-content > .notif-icon").removeClass("failed fas fas-times fa-check");
            $("#ModalUpdate .modal-content > .notif-icon").addClass("failed fas fa-times");
            $("#ModalUpdate .modal-content > .notif-desc").html("Failed download selected installer. Error: <b>" + get_ajax_error(xhr, err) + "</b>");
            $("#ModalUpdate .modal-content > .notif-btn").removeClass("failed");
            $("#ModalUpdate .modal-content > .notif-btn").addClass("failed");
            $('#ModalUpdate').modal('show');
          }
        }); 
      });

      $(".btn-download").click(function()
      {
        var installer = [];
        var join_installer;

        $(".selected_installer:checked").each(function() 
        {
          installer.push($(this).val());
        });

        join_installer = installer.join('#') ;

        $('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
        $.ajax({
          type: "POST",
          url: "create_iso.php",
          data: {"check_all_requirement" : true, "data" : join_installer},
          cache: false,
          success: function(data)
          { 
            var data = JSON.parse(data);
            console.log(data);

            $('.loading-indication').remove();

            if(data.log == "0")
            {
              create_iso();
            }
            else
            {
              var undownloaded = [];
              var message = data.errdesc;
              
              if(typeof data.list !== 'undefined')
              {
                message += " <br> List: ";

                for (let i = 0; i < data.list.length; i++) 
                {
                  message += data.list[i]["filename"] + ", "; 
                  undownloaded.push(data.list[i]["data"]);
                }

                message = message.substr(0, (message.length - 2));
              }

              $("#ModalCheck .modal-content > .notif-title").html("TERJADI KESALAHAN!");
              $("#ModalCheck .modal-content > .notif-icon").removeClass("failed fas fas-times fa-check");
              $("#ModalCheck .modal-content > .notif-icon").addClass("failed fas fa-times");
              $("#ModalCheck .modal-content > .notif-desc").html(message);
              $("#ModalCheck .modal-content > .notif-btn").removeClass("failed");
              $("#ModalCheck .modal-content > .notif-btn").addClass("failed");

              if(data.log == "check_104")
              {
                $("#ModalCheck .modal-content > .notif-btn").html("UNDUH");
                $("#ModalCheck .notif-btn").click(function(e)
                {
                  download_file(undownloaded, "update_and_convert");
                });
              }

              $('#ModalCheck').modal('show');
            }

          },
          error: function(xhr, err)
          {
            $('.loading-indication').remove();
            $("#ModalCheck .modal-content > .notif-title").html("TERJADI KESALAHAN!");
            $("#ModalCheck .modal-content > .notif-icon").removeClass("failed fas fas-times fa-check");
            $("#ModalCheck .modal-content > .notif-icon").addClass("failed fas fa-times");
            $("#ModalCheck .modal-content > .notif-desc").html("Gagal cek file installer. Error: <b>" + get_ajax_error(xhr, err) + "</b>");
            $("#ModalCheck .modal-content > .notif-btn").removeClass("failed");
            $("#ModalCheck .modal-content > .notif-btn").addClass("failed");
            $('#ModalCheck').modal('show');
          }
        }); 
      });
    });

    function download_file (data, status = "just_update")
    {
      $('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');
      $.ajax({
        type: "POST",
        url: "create_iso.php",
        data: {"download_installer" : true, "data" : data},
        cache: false,
        success: function(data)
        {
          var data = JSON.parse(data);
          console.log(data);

          $('.loading-indication').remove();

          if(data.log == "0")
          {
            if(status == "update_and_convert")
            {
              create_iso();             
            }
            if(status == "just_update")
            {             
              $("#ModalUpdate .modal-content > .notif-title").html("SUKSES!");
              $("#ModalUpdate .modal-content > .notif-icon").removeClass("failed fas fa-times fa-check");
              $("#ModalUpdate .modal-content > .notif-icon").addClass("fas fa-check");
              $("#ModalUpdate .modal-content > .notif-desc").html("Berhasil update semua installer.");
              $("#ModalUpdate .modal-content > .notif-btn").removeClass("failed");
              $('#ModalUpdate').modal('show');
            }
          }
          else
          {
            var message = data.errdesc;

            if(typeof data.list["success"] !== 'undefined')
            {
              message += " <br> List Berhasil: ";

              for (let i = 0; i < data.list["success"].length; i++) 
              {
                message += data.list["success"][i] + ", "; 
              }

              message = message.substr(0, (message.length - 2));
            }
            
            if(typeof data.list["failed"] !== 'undefined')
            {
              message += " <br> List Gagal: ";

              for (let i = 0; i < data.list["failed"].length; i++) 
              {
                message += data.list["failed"][i] + ", ";
              }

              message = message.substr(0, (message.length - 2));
            }

            $("#ModalProcess .modal-content > .notif-title").html("TERJADI KESALAHAN!");
            $("#ModalProcess .modal-content > .notif-icon").removeClass("failed fas fas-times fa-check");
            $("#ModalProcess .modal-content > .notif-icon").addClass("failed fas fa-times");
            $("#ModalProcess .modal-content > .notif-desc").html(message);
            $("#ModalProcess .modal-content > .notif-btn").removeClass("failed");
            $("#ModalProcess .modal-content > .notif-btn").addClass("failed");
            $('#ModalProcess').modal('show');
          }

        },
        error: function(xhr, err)
        {
          $('.loading-indication').remove();
          $("#ModalProcess .modal-content > .notif-title").html("TERJADI KESALAHAN!");
          $("#ModalProcess .modal-content > .notif-icon").removeClass("failed fas fas-times fa-check");
          $("#ModalProcess .modal-content > .notif-icon").addClass("failed fas fa-times");
          $("#ModalProcess .modal-content > .notif-desc").html("Gagal download file installer. Error: <b>" + get_ajax_error(xhr, err) + "</b>");
          $("#ModalProcess .modal-content > .notif-btn").removeClass("failed");
          $("#ModalProcess .modal-content > .notif-btn").addClass("failed");
          $('#ModalProcess').modal('show');
        }
      }); 
    }

    function create_iso()
    {
      var installer = [];
      var join_installer;

      $(".selected_installer:checked").each(function() 
      {
        installer.push($(this).val());
      });

      join_installer = installer.join('#') ;

      $('body').prepend('<div class="loading-indication"><img src="assets/img/loading.gif"/><div class="bg-loading"></div></div>');

      $.ajax({
        type: "POST",
        url: "create_iso.php",
        data: {"create_iso" : true, "data" : join_installer},
        cache: false,
        success: function(data)
        {
          console.log(data);
          var data = JSON.parse(data);
          console.log(data);

          $('.loading-indication').remove();

          if(data.log == "0")
          {
            $("#ModalResult .modal-content > .notif-title").html("SUKSES!");
            $("#ModalResult .modal-content > .notif-icon").removeClass("failed fas fa-times fa-check");
            $("#ModalResult .modal-content > .notif-icon").addClass("fas fa-check");
            $("#ModalResult .modal-content > .notif-desc").html("Berhasil membuat ISO.");
            $("#ModalResult .modal-content > .notif-btn").removeClass("failed");
            $('#ModalResult').modal('show');
            load_iso_list();
          }
          else
          {
            var message = data.errdesc;

            if(typeof data.list["success"] !== 'undefined')
            {
              message += " <br> List Berhasil: ";

              for (let i = 0; i < data.list["success"].length; i++) 
              {
                message += data.list["success"][i] + ", "; 
              }

              message = message.substr(0, (message.length - 2));
            }
            
            if(typeof data.list["failed"] !== 'undefined')
            {
              message += " <br> List Gagal: ";

              for (let i = 0; i < data.list["failed"].length; i++) 
              {
                message += data.list["failed"][i] + ", ";
              }

              message = message.substr(0, (message.length - 2));
            }

            $("#ModalResult .modal-content > .notif-title").html("TERJADI KESALAHAN!");
            $("#ModalResult .modal-content > .notif-icon").removeClass("failed fas fas-times fa-check");
            $("#ModalResult .modal-content > .notif-icon").addClass("failed fas fa-times");
            $("#ModalResult .modal-content > .notif-desc").html(message);
            $("#ModalResult .modal-content > .notif-btn").removeClass("failed");
            $("#ModalResult .modal-content > .notif-btn").addClass("failed");
            $('#ModalResult').modal('show');
          }
        },
        error: function(xhr, err)
        {
          $('.loading-indication').remove();
          $("#ModalResult .modal-content > .notif-title").html("TERJADI KESALAHAN!");
          $("#ModalResult .modal-content > .notif-icon").removeClass("failed fas fas-times fa-check");
          $("#ModalResult .modal-content > .notif-icon").addClass("failed fas fa-times");
          $("#ModalResult .modal-content > .notif-desc").html("Gagal membuat ISO. Error: <b>" + get_ajax_error(xhr, err) + "</b>");
          $("#ModalResult .modal-content > .notif-btn").removeClass("failed");
          $("#ModalResult .modal-content > .notif-btn").addClass("failed");
          $('#ModalResult').modal('show');
        }
      }); 
    }

    function load_iso_list()
    {        
      $.ajax({
        type: "POST",
        url: "index.php",
        data: {"load_iso_list" : true},
        cache: false,
        success: function(data)
        {
          $("#ResponseLoadIso").html(data);
        },
        error: function(xhr, err)
        {
          alert("Terjadi kesalahan saat memuat list ISO, error: " + get_ajax_error(xhr, err));
        }
      }); 
    }
    </script>
	</body>
</html>
<?php
}
?>